﻿namespace EMPS.Domain
{
    public class EmployeeInfo
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
        
        public decimal AnnualSalary { get; set; }
        
        public int SuperRateInPercent { get; set; }
        
        public string PayPeriod { get; set; }
    }
}