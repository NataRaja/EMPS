﻿using EMPS.Application.Core.Result;
using EMPS.Application.GrossIncome;
using EMPS.Application.IncomeTax;
using EMPS.Application.NetIncome;
using EMPS.Application.PayPeriod;
using EMPS.Application.SuperRate;
using EMPS.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace EMPS.Application.Tests;

public class MonthlyPaySlipServiceTests
{
    [Fact]
    public void ShouldReturnSuccessfulResult()
    {
        var result = GetSubject().Calculate(new EmployeeInfo
        {
            FirstName = "FirstName",
            LastName = "LastName",
            AnnualSalary = 10000,
            PayPeriod = "June",
            SuperRateInPercent = 7
        });

        result.Should().BeEquivalentTo(Result<PaySlip>.Success(new PaySlip
        {
            Name = "FirstName LastName",
            PayPeriod = "01 June - 30 June",
            GrossIncome = 4000,
            IncomeTax = 12000,
            NetIncome = 900,
            SuperRate = 3000
        }));
    }

    [Theory]
    [InlineData(FailedService.PayPeriodService)]
    [InlineData(FailedService.GrossIncomeService)]
    [InlineData(FailedService.IncomeTaxService)]
    [InlineData(FailedService.NetIncomeService)]
    [InlineData(FailedService.SuperRateService)]
    public void ShouldReturnFailedResult(FailedService failedService)
    {
        var result = GetSubject(failedService).Calculate(new EmployeeInfo
        {
            FirstName = "FirstName",
            LastName = "LastName",
            AnnualSalary = 10000,
            PayPeriod = "June",
            SuperRateInPercent = 7
        });

        result.Failed.Should().BeTrue();
        result.ErrorMessage.Should().Be("error");
    }

    private MonthlyPaySlipService GetSubject(FailedService failedService = FailedService.None)
    {
        var payPeriodService = new Mock<IPayPeriodService>();
        payPeriodService.Setup(x => x.Get(It.IsAny<string>()))
            .Returns(failedService == FailedService.PayPeriodService
                ? Result<string>.Fail("error")
                : Result<string>.Success("01 June - 30 June"));

        var grossIncomeService = new Mock<IGrossIncomeService>();
        grossIncomeService.Setup(x => x.Get(It.IsAny<decimal>()))
            .Returns(failedService == FailedService.GrossIncomeService
                ? Result<decimal>.Fail("error")
                : Result<decimal>.Success(4000));

        var incomeTaxService = new Mock<IIncomeTaxService>();
        incomeTaxService.Setup(x => x.Get(It.IsAny<decimal>()))
            .Returns(failedService == FailedService.IncomeTaxService
                ? Result<decimal>.Fail("error")
                : Result<decimal>.Success(12000));

        var netIncomeService = new Mock<INetIncomeService>();
        netIncomeService.Setup(x => x.Get(It.IsAny<decimal>(), It.IsAny<decimal>()))
            .Returns(failedService == FailedService.NetIncomeService
                ? Result<decimal>.Fail("error")
                : Result<decimal>.Success(900));

        var superRateService = new Mock<ISuperRateService>();
        superRateService.Setup(x => x.Get(It.IsAny<decimal>(), It.IsAny<decimal>()))
            .Returns(failedService == FailedService.SuperRateService
                ? Result<decimal>.Fail("error")
                : Result<decimal>.Success(3000));

        return new MonthlyPaySlipService(
            payPeriodService.Object,
            grossIncomeService.Object,
            incomeTaxService.Object,
            netIncomeService.Object,
            superRateService.Object
        );
    }

    public enum FailedService
    {
        None,
        PayPeriodService,
        GrossIncomeService,
        IncomeTaxService,
        NetIncomeService,
        SuperRateService
    }
}