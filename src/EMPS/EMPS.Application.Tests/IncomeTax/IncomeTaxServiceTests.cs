﻿using EMPS.Application.IncomeTax;
using FluentAssertions;
using Moq;
using Xunit;

namespace EMPS.Application.Tests.IncomeTax;

public class IncomeTaxServiceTests
{
    private readonly IncomeTaxService _subject;

    public IncomeTaxServiceTests()
    {
        var rateProvider = new Mock<IRateProvider>();
        rateProvider.SetupGet(x => x.RateItems).Returns(new List<RateItem>
        {
            new() { MaxIncome = 20000, RateInPercent = 5 },
            new() { MaxIncome = 50000, RateInPercent = 7 }
        });
        rateProvider.SetupGet(x => x.RemainingIncomeRateInPercent).Returns(20);

        _subject = new IncomeTaxService(rateProvider.Object);
    }

    [Theory]
    [InlineData(15000, 62.5)]
    [InlineData(40000, 200)]
    [InlineData(100000, 1091.67)]
    public void ShouldReturnCorrectResult(decimal annualSalary, decimal expectedResult)
    {
        var result = _subject.Get(annualSalary);

        result.Succeeded.Should().BeTrue();
        Math.Round(result.Data, 2).Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(-10)]
    public void ShouldReturnFailedResult(decimal annualSalary)
    {
        var result = _subject.Get(annualSalary);

        result.Failed.Should().BeTrue();
        result.ErrorMessage.Should().Be("The annualSalary should be positive");
    }
}