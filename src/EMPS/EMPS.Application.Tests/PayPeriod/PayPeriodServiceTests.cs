﻿using EMPS.Application.PayPeriod;
using FluentAssertions;
using Xunit;

namespace EMPS.Application.Tests.PayPeriod;

public class PayPeriodServiceTests
{
    private readonly PayPeriodService _subject;

    public PayPeriodServiceTests()
    {
        _subject = new PayPeriodService();
    }

    [Theory]
    [InlineData("January", "01 January - 31 January")]
    [InlineData("March", "01 March - 31 March")]
    [InlineData("April", "01 April - 30 April")]
    [InlineData("May", "01 May - 31 May")]
    [InlineData("June", "01 June - 30 June")]
    [InlineData("July", "01 July - 31 July")]
    [InlineData("August", "01 August - 31 August")]
    [InlineData("September", "01 September - 30 September")]
    [InlineData("October", "01 October - 31 October")]
    [InlineData("November", "01 November - 30 November")]
    [InlineData("December", "01 December - 31 December")]
    [MemberData(nameof(DynamicMonthTestData))]
    public void ShouldReturnCorrectPayPeriod(string monthName, string expectedResult)
    {
        var result = _subject.Get(monthName);

        result.Succeeded.Should().BeTrue();
        result.Data.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(null, "The monthName can not be null or empty")]
    [InlineData("", "The monthName can not be null or empty")]
    [InlineData("NonameMonth", "The monthName has an incorrect value")]
    public void ShouldReturnFailedResult(string monthName, string expectedErrorMessage)
    {
        var result = _subject.Get(monthName);
        
        result.Failed.Should().BeTrue();
        result.ErrorMessage.Should().Be(expectedErrorMessage);
    }
    
    public static IEnumerable<object[]> DynamicMonthTestData()
    {
        var currentDate = DateTime.Now;
        var firstDayOfFebruary = new DateTime(currentDate.Year, 02, 1);
        var lastDayOfFebruary = firstDayOfFebruary.AddMonths(1).AddDays(-1);

        yield return new object[]
            { "February", $"{firstDayOfFebruary.Day:00} February - {lastDayOfFebruary.Day:00} February" };
    }
}