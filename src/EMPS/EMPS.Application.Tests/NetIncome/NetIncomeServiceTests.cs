﻿using EMPS.Application.NetIncome;
using FluentAssertions;
using Xunit;

namespace EMPS.Application.Tests.NetIncome;

public class NetIncomeServiceTests
{
    private readonly NetIncomeService _subject;

    public NetIncomeServiceTests()
    {
        _subject = new NetIncomeService();
    }
    
    [Fact]
    public void ShouldReturnCorrectResult()
    {
        var result = _subject.Get(1000, 200);

        result.Succeeded.Should().BeTrue();
        result.Data.Should().Be(800);
    }

    [Theory]
    [InlineData(0, 20, "The grossIncome should be positive")]
    [InlineData(-10, 20, "The grossIncome should be positive")]
    [InlineData(30, 0, "The incomeTax should be positive")]
    [InlineData(30, -20, "The incomeTax should be positive")]
    public void ShouldReturnFailedResult(decimal grossIncome, decimal incomeTax, string errorMessage)
    {
        var result = _subject.Get(grossIncome, incomeTax);

        result.Failed.Should().BeTrue();
        result.ErrorMessage.Should().Be(errorMessage);
    }
}