﻿using EMPS.Application.SuperRate;
using FluentAssertions;
using Xunit;

namespace EMPS.Application.Tests.SuperRate;

public class SuperRateServiceTests
{
    private readonly SuperRateService _subject;

    public SuperRateServiceTests()
    {
        _subject = new SuperRateService();
    }
    
    [Fact]
    public void ShouldReturnCorrectResult()
    {
        var result = _subject.Get(1000, 9);

        result.Succeeded.Should().BeTrue();
        result.Data.Should().Be(90);
    }

    [Theory]
    [InlineData(0, 7, "The grossIncome should be positive")]
    [InlineData(-10, 7, "The grossIncome should be positive")]
    [InlineData(1000, 0, "The superRateInPercent should be positive")]
    [InlineData(1000, -20, "The superRateInPercent should be positive")]
    public void ShouldReturnFailedResult(decimal grossIncome, decimal superRateInPercent, string errorMessage)
    {
        var result = _subject.Get(grossIncome, superRateInPercent);

        result.Failed.Should().BeTrue();
        result.ErrorMessage.Should().Be(errorMessage);
    }
}