﻿using EMPS.Application.GrossIncome;
using FluentAssertions;
using Xunit;

namespace EMPS.Application.Tests.GrossIncome;

public class GrossIncomeServiceTests
{
    private readonly GrossIncomeService _subject;

    public GrossIncomeServiceTests()
    {
        _subject = new GrossIncomeService();
    }

    [Fact]
    public void ShouldReturnCorrectGrossIncomePerMonth()
    {
        var result = _subject.Get(12000);

        result.Succeeded.Should().BeTrue();
        result.Data.Should().Be(1000);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(-10)]
    public void ShouldReturnFailedResult(decimal annualSalary)
    {
        var result = _subject.Get(annualSalary);
        
        result.Failed.Should().BeTrue();
        result.ErrorMessage.Should().Be($"The annualSalary should be positive");
    }
}