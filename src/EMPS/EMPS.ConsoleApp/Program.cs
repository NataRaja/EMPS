﻿using EMPS.Application;
using EMPS.Application.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

Console.WriteLine("*Employee monthly pay slip*");

var serviceProvider = new ServiceCollection()
    .AddLogging()
    .AddInfrastructureServices()
    .AddApplicationServices()
    .BuildServiceProvider();
var logger = serviceProvider.GetService<ILoggerFactory>()
    .CreateLogger<Program>();

var runner = new ApplicationRunner(serviceProvider, logger);

var currentDirectory = Directory.GetCurrentDirectory();

var defaultInputFilePath = @$"{currentDirectory}\inputFile.csv";
Console.WriteLine($"Enter input file path({defaultInputFilePath}):");
var inputFilepath = Console.ReadLine() ?? defaultInputFilePath;
Console.WriteLine(inputFilepath);

var defaultOutputFilePath = @$"{currentDirectory}\outputFile.csv";
Console.WriteLine($"Enter output file path({defaultOutputFilePath}):");
var outputFilepath = Console.ReadLine() ?? defaultOutputFilePath;
Console.WriteLine(outputFilepath);

Console.WriteLine("The process is running...");

await runner.Run(
    !string.IsNullOrEmpty(inputFilepath) ? inputFilepath : defaultInputFilePath,
    !string.IsNullOrEmpty(outputFilepath) ? outputFilepath : defaultOutputFilePath
);

Console.WriteLine("The process has been completed successfully.");
Console.WriteLine("Press any key to exit...");
Console.ReadKey();