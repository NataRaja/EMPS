﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using EMPS.Domain;

namespace EMPS.Infrastructure
{
    public class CsvDataWriter : IDataWriter
    {
        public async Task Write(IEnumerable<PaySlip> data, string filePath)
        {
            if (File.Exists(filePath)) File.Delete(filePath);
            
            var configuration = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Encoding = Encoding.UTF8,
                Delimiter = ","
            };

            await using var fileStream = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.Write);
            await using var streamWriter = new StreamWriter(fileStream);
            await using var csvWriter = new CsvWriter(streamWriter, configuration);
            csvWriter.Context.RegisterClassMap<PaySlipMapper>(); 
            await csvWriter.WriteRecordsAsync(data);
        }
    }
}