﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EMPS.Domain;

namespace EMPS.Infrastructure
{
    public interface IDataReader
    {
        Task<IEnumerable<EmployeeInfo>> Read(string filePath);
    }
}