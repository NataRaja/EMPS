﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EMPS.Domain;

namespace EMPS.Infrastructure
{
    public interface IDataWriter
    {
        Task Write(IEnumerable<PaySlip> data, string filePath);
    }
}