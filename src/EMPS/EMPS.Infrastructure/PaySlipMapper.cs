﻿using CsvHelper.Configuration;
using EMPS.Domain;

namespace EMPS.Infrastructure
{
    public sealed class PaySlipMapper : ClassMap<PaySlip>
    {
        private const string DecimalFormat = "0.00";
        
        public PaySlipMapper()
        {
            Map(m => m.Name).Name("name");
            Map(m => m.PayPeriod).Name("pay period");
            Map(m => m.GrossIncome).Name("gross income").TypeConverterOption.Format(DecimalFormat);
            Map(m => m.IncomeTax).Name("income tax").TypeConverterOption.Format(DecimalFormat);
            Map(m => m.NetIncome).Name("net income").TypeConverterOption.Format(DecimalFormat);
            Map(m => m.SuperRate).Name("super").TypeConverterOption.Format(DecimalFormat);
        }
    }
}