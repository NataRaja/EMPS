﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using EMPS.Domain;

namespace EMPS.Infrastructure
{
    public class CsvDataReader : IDataReader
    {
        public async Task<IEnumerable<EmployeeInfo>> Read(string filePath)
        {
            if (!File.Exists(filePath)) throw new Exception("File does not exist");

            var configuration = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Encoding = Encoding.UTF8,
                Delimiter = ","
            };

            await using var fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            using var streamReader = new StreamReader(fileStream, Encoding.UTF8);
            using var csvReader = new CsvReader(streamReader, configuration);
            {
                csvReader.Context.RegisterClassMap<EmployeeInfoMapper>();
                
                var data = csvReader.GetRecordsAsync<EmployeeInfo>();

                var result = new List<EmployeeInfo>();
                await foreach (var item in data)
                {
                    result.Add(item);
                }

                return result;
            }
        }
    }
}