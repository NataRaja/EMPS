﻿using CsvHelper.Configuration;
using EMPS.Domain;

namespace EMPS.Infrastructure
{
    public sealed class EmployeeInfoMapper : ClassMap<EmployeeInfo>
    {
        public EmployeeInfoMapper()
        {
            Map(m => m.FirstName).Index(0);
            Map(m => m.LastName).Index(1);
            Map(m => m.AnnualSalary).Index(2);
            Map(m => m.SuperRateInPercent).Index(3);
            Map(m => m.PayPeriod).Index(4);
        }
    }
}