﻿using EMPS.Application.Core.Result;

namespace EMPS.Application.IncomeTax
{
    public interface IIncomeTaxService
    {
        IResult<decimal> Get(decimal annualSalary);
    }
}