﻿using System.Collections.Generic;

namespace EMPS.Application.IncomeTax
{
    public class StaticRateProvider : IRateProvider
    {
        public decimal RemainingIncomeRateInPercent => 39m;

        public IEnumerable<RateItem> RateItems
        {
            get
            {
                yield return new RateItem { MaxIncome = 14000, RateInPercent = 10.5m };
                yield return new RateItem { MaxIncome = 48000, RateInPercent = 17.5m };
                yield return new RateItem { MaxIncome = 70000, RateInPercent = 30m };
                yield return new RateItem { MaxIncome = 180000, RateInPercent = 33m };
            }
        }
    }
}