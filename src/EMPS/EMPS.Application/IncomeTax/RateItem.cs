﻿namespace EMPS.Application.IncomeTax
{
    public class RateItem
    {
        public decimal MaxIncome { get; set; }
        public decimal RateInPercent { get; set; }
    }
}