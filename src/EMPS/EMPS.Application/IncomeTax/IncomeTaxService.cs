﻿using System.Collections.Generic;
using System.Linq;
using EMPS.Application.Core.Result;

namespace EMPS.Application.IncomeTax
{
    public class IncomeTaxService : IIncomeTaxService
    {
        private readonly IRateProvider _rateProvider;

        public IncomeTaxService(IRateProvider rateProvider)
        {
            _rateProvider = rateProvider;
        }
        
        public IResult<decimal> Get(decimal annualSalary)
        {
            if (!IsValid(annualSalary)) return Result<decimal>.Fail($"The {nameof(annualSalary)} should be positive");

            var result = CalculateIncomeTax(_rateProvider.RateItems, annualSalary);

            var incomeTax = (result.RemainingIncome > 0
                ? result.Data + GetIncomeTax(result.RemainingIncome, _rateProvider.RemainingIncomeRateInPercent)
                : result.Data) / 12;

            return Result<decimal>.Success(incomeTax);
        }

        private static bool IsValid(decimal annualSalary)
        {
            return annualSalary > 0;
        }

        private static IncomeResult CalculateIncomeTax(IEnumerable<RateItem> rateItems, decimal annualSalary)
        {
            decimal data = 0;
            decimal previousMaxIncome = 0;

            var currentIncome = annualSalary;
            foreach (var rateItem in rateItems.OrderBy(x => x.MaxIncome))
            {
                var incomeItem = rateItem.MaxIncome - previousMaxIncome;
                var income = currentIncome - incomeItem;

                if (income > 0)
                {
                    data += GetIncomeTax(incomeItem, rateItem.RateInPercent);
                    currentIncome -= incomeItem;
                }
                else
                {
                    data += GetIncomeTax(currentIncome, rateItem.RateInPercent);

                    return new IncomeResult(data, 0);
                }

                previousMaxIncome = rateItem.MaxIncome;
            }

            return new IncomeResult(data, currentIncome);
        }

        private static decimal GetIncomeTax(decimal income, decimal rate)
        {
            return income * rate / 100;
        }

        private class IncomeResult
        {
            public IncomeResult(decimal data, decimal remainingIncome)
            {
                Data = data;
                RemainingIncome = remainingIncome;
            }

            public decimal Data { get; }

            public decimal RemainingIncome { get; }
        }
    }
}