﻿using System.Collections.Generic;

namespace EMPS.Application.IncomeTax
{
    public interface IRateProvider
    {
        decimal RemainingIncomeRateInPercent { get; }
        IEnumerable<RateItem> RateItems { get; }
    }
}