﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EMPS.Domain;
using EMPS.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace EMPS.Application
{
    public class ApplicationRunner
    {
        private readonly ServiceProvider _serviceProvider;
        private readonly ILogger _logger;

        public ApplicationRunner(ServiceProvider serviceProvider, ILogger logger)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        public async Task Run(string inputFilePath, string outputFilePath)
        {
            var inputData = await ReadData(inputFilePath);
            var outputData = GetPaySlips(inputData);
            await WriteData(outputData, outputFilePath);
        }

        private async Task<IEnumerable<EmployeeInfo>> ReadData(string inputFilePath)
        {
            var dataReader = _serviceProvider.GetService<IDataReader>();

            return await dataReader.Read(inputFilePath);
        }

        private IEnumerable<PaySlip> GetPaySlips(IEnumerable<EmployeeInfo> data)
        {
            var result = new List<PaySlip>();
            var lineCounter = 1;

            var monthlyPaySlipService = _serviceProvider.GetService<IMonthlyPaySlipService>();

            foreach (var employeeInfo in data)
            {
                var paySlipResult = monthlyPaySlipService.Calculate(employeeInfo);
                if (paySlipResult.Succeeded) result.Add(paySlipResult.Data);
                else
                {
                    _logger.LogError($"Error: line #{lineCounter}: {paySlipResult.ErrorMessage}");
                }

                lineCounter++;
            }

            return result;
        }

        private async Task WriteData(IEnumerable<PaySlip> data, string outputFilePath)
        {
            var dataWriter = _serviceProvider.GetService<IDataWriter>();
            await dataWriter.Write(data, outputFilePath);
        }
    }
}