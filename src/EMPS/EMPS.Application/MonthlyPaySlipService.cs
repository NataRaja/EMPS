﻿using EMPS.Application.Core.Result;
using EMPS.Application.GrossIncome;
using EMPS.Application.IncomeTax;
using EMPS.Application.NetIncome;
using EMPS.Application.PayPeriod;
using EMPS.Application.SuperRate;
using EMPS.Domain;

namespace EMPS.Application
{
    public class MonthlyPaySlipService : IMonthlyPaySlipService
    {
        private readonly IPayPeriodService _payPeriodService;
        private readonly IGrossIncomeService _grossIncomeService;
        private readonly IIncomeTaxService _incomeTaxService;
        private readonly INetIncomeService _netIncomeService;
        private readonly ISuperRateService _superRateService;

        public MonthlyPaySlipService(
            IPayPeriodService payPeriodService,
            IGrossIncomeService grossIncomeService,
            IIncomeTaxService incomeTaxService,
            INetIncomeService netIncomeService,
            ISuperRateService superRateService)
        {
            _payPeriodService = payPeriodService;
            _grossIncomeService = grossIncomeService;
            _incomeTaxService = incomeTaxService;
            _netIncomeService = netIncomeService;
            _superRateService = superRateService;
        }

        public IResult<PaySlip> Calculate(EmployeeInfo employeeInfo)
        {
            var result = new PaySlip
            {
                Name = $"{employeeInfo.FirstName} {employeeInfo.LastName}"
            };

            var payPeriod = _payPeriodService.Get(employeeInfo.PayPeriod);
            if (payPeriod.Succeeded) result.PayPeriod = payPeriod.Data;
            else return Result<PaySlip>.Fail(payPeriod.ErrorMessage);

            var grossIncome = _grossIncomeService.Get(employeeInfo.AnnualSalary);
            if (grossIncome.Succeeded) result.GrossIncome = grossIncome.Data;
            else return Result<PaySlip>.Fail(grossIncome.ErrorMessage);

            var incomeTax = _incomeTaxService.Get(employeeInfo.AnnualSalary);
            if (incomeTax.Succeeded) result.IncomeTax = incomeTax.Data;
            else return Result<PaySlip>.Fail(incomeTax.ErrorMessage);

            var netIncome = _netIncomeService.Get(result.GrossIncome, result.IncomeTax);
            if (netIncome.Succeeded) result.NetIncome = netIncome.Data;
            else return Result<PaySlip>.Fail(netIncome.ErrorMessage);

            var superRate = _superRateService.Get(result.GrossIncome, employeeInfo.SuperRateInPercent);
            if (superRate.Succeeded) result.SuperRate = superRate.Data;
            else return Result<PaySlip>.Fail(superRate.ErrorMessage);

            return Result<PaySlip>.Success(result);
        }
    }
}