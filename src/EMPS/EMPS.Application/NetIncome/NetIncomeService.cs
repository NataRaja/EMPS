﻿using EMPS.Application.Core.Result;

namespace EMPS.Application.NetIncome
{
    public class NetIncomeService : INetIncomeService
    {
        public IResult<decimal> Get(decimal grossIncome, decimal incomeTax)
        {
            if (!IsValid(grossIncome)) return Result<decimal>.Fail($"The {nameof(grossIncome)} should be positive");
            if (!IsValid(incomeTax)) return Result<decimal>.Fail($"The {nameof(incomeTax)} should be positive");

            return Result<decimal>.Success(grossIncome - incomeTax);
        }

        private static bool IsValid(decimal value)
        {
            return value > 0;
        }
    }
}