﻿using EMPS.Application.Core.Result;

namespace EMPS.Application.NetIncome
{
    public interface INetIncomeService
    {
        IResult<decimal> Get(decimal grossIncome, decimal incomeTax);
    }
}