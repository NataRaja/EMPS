﻿using System;
using System.Globalization;
using EMPS.Application.Core.Result;

namespace EMPS.Application.PayPeriod
{
    public class PayPeriodService : IPayPeriodService
    {
        public IResult<string> Get(string monthName)
        {
            if (!IsValid(monthName)) return Result<string>.Fail($"The {nameof(monthName)} can not be null or empty");

            if (DateTime.TryParseExact(monthName, "MMMM",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out var date))
            {
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                return Result<string>.Success($"{firstDayOfMonth.Day:00} {monthName} - {lastDayOfMonth.Day} {monthName}");
            }

            return Result<string>.Fail($"The {nameof(monthName)} has an incorrect value");
        }

        private static bool IsValid(string monthName)
        {
            return !string.IsNullOrEmpty(monthName);
        }
    }
}