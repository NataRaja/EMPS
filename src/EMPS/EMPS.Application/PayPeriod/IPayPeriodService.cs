﻿using EMPS.Application.Core.Result;

namespace EMPS.Application.PayPeriod
{
    public interface IPayPeriodService
    {
        IResult<string> Get(string monthName);
    }
}