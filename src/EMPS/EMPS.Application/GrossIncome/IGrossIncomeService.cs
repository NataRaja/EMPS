﻿using EMPS.Application.Core.Result;

namespace EMPS.Application.GrossIncome
{
    public interface IGrossIncomeService
    {
        IResult<decimal> Get(decimal annualSalary);
    }
}