﻿using EMPS.Application.Core.Result;

namespace EMPS.Application.GrossIncome
{
    public class GrossIncomeService: IGrossIncomeService
    {
        public IResult<decimal> Get(decimal annualSalary)
        {
            if (!IsValid(annualSalary)) return Result<decimal>.Fail($"The {nameof(annualSalary)} should be positive");
            
            return Result<decimal>.Success(annualSalary/12);
        }
        
        private static bool IsValid(decimal annualSalary)
        {
            return annualSalary > 0;
        }
    }
}