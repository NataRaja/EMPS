﻿using EMPS.Application.Core.Result;

namespace EMPS.Application.SuperRate
{
    public interface ISuperRateService
    {
        IResult<decimal> Get(decimal grossIncome, decimal superRateInPercent);
    }
}