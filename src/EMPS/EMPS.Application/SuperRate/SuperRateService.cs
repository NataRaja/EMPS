﻿using EMPS.Application.Core.Result;

namespace EMPS.Application.SuperRate
{
    public class SuperRateService : ISuperRateService
    {
        public IResult<decimal> Get(decimal grossIncome, decimal superRateInPercent)
        {
            if (!IsValid(grossIncome)) return Result<decimal>.Fail($"The {nameof(grossIncome)} should be positive");
            if (!IsValid(superRateInPercent))
                return Result<decimal>.Fail($"The {nameof(superRateInPercent)} should be positive");

            return Result<decimal>.Success(grossIncome * superRateInPercent / 100);
        }

        private static bool IsValid(decimal value)
        {
            return value > 0;
        }
    }
}