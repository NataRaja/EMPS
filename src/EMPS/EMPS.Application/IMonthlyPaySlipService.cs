﻿using EMPS.Application.Core.Result;
using EMPS.Domain;

namespace EMPS.Application
{
    public interface IMonthlyPaySlipService
    {
        IResult<PaySlip> Calculate(EmployeeInfo employeeInfo);
    }
}