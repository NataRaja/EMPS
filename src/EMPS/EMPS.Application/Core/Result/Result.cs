﻿namespace EMPS.Application.Core.Result
{
    public class Result<T> : IResult<T>
    {
        protected Result()
        {
        }

        public bool Failed => !Succeeded;

        public string ErrorMessage { get; private set; }

        public bool Succeeded { get; private set; }

        public T Data { get; private set; }

        public static IResult<T> Fail()
        {
            var result = new Result<T>
            {
                Succeeded = false
            };

            return result;
        }

        public static IResult<T> Fail(string errorMessage)
        {
            var result = new Result<T>
            {
                Succeeded = false,
                ErrorMessage = errorMessage
            };

            return result;
        }

        public static IResult<T> Success()
        {
            var result = new Result<T>
            {
                Succeeded = true
            };

            return result;
        }

        public static IResult<T> Success(T data)
        {
            var result = new Result<T>
            {
                Succeeded = true,
                Data = data
            };

            return result;
        }

        public static IResult<T> Success(T data, string message)
        {
            var result = new Result<T>
            {
                Succeeded = true,
                Data = data,
                ErrorMessage = message
            };

            return result;
        }
    }
}