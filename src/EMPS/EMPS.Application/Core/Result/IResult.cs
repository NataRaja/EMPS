﻿namespace EMPS.Application.Core.Result
{
    public interface IResult<out T>
    {
        bool Failed { get; }

        string ErrorMessage { get; }

        bool Succeeded { get; }
        
        T Data { get; }
    }
}