﻿using EMPS.Application.GrossIncome;
using EMPS.Application.IncomeTax;
using EMPS.Application.NetIncome;
using EMPS.Application.PayPeriod;
using EMPS.Application.SuperRate;
using EMPS.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace EMPS.Application.Core
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddScoped<IMonthlyPaySlipService, MonthlyPaySlipService>()
                .AddScoped<IPayPeriodService, PayPeriodService>()
                .AddScoped<IGrossIncomeService, GrossIncomeService>()
                .AddScoped<IIncomeTaxService, IncomeTaxService>()
                .AddScoped<INetIncomeService, NetIncomeService>()
                .AddScoped<ISuperRateService, SuperRateService>()
                .AddScoped<IRateProvider, StaticRateProvider>();
        }

        public static IServiceCollection AddInfrastructureServices(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddScoped<IDataReader, CsvDataReader>()
                .AddScoped<IDataWriter, CsvDataWriter>();
        }
    }
}