# Employee monthly pay slip

## Introduction
This repository contains the .net application that will help you to calculat an employee monthly pay slip. This is a simple solution(MVP) and can be improved/extended by using other implementation of the interfaces.

The questions that I've decided not to consider in this solution:
1. Should we handle a large input file? It that case we can handle data partialy bt using batching.
2. Should the user provide a year? Or should we use the current year always? So, it's a business decision.
3. How can we provide rate table in the application? It might be a settings in the appSettings file or something else. Anyway, I've provided an apportunity to expand this.

## Input data example
The user has to create a file with a template like this:

#### CSV file
```
first name,last name,annual salary,super rate (%),pay period
John,Smith,60050,9,March
Alex,Wong,120000,10%,March
```

## How to run an application?

1. Build the `EMPS.ConsoleApp` project.
2. Run the `EMPS.ConsoleApp` project.
3. Provide the full path to the file that you would like to handle. The default path is shown in brackets.
4. Provide the full path to the file to which the application will save the result.